﻿using ConsumindoAPIRandomUsers.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Net;

namespace ConsumindoAPIRandomUsers
{
    class Program
    {
        static void Main(string[] args)
        {


            bool isValidNumUsers;
            string nat;

            Console.WriteLine("Digite o Numero do usuarios desejados:");

            isValidNumUsers = int.TryParse(Console.ReadLine(), out int numero);

            if (!isValidNumUsers)
                numero = 1;

            Console.WriteLine("Digite a nacionalidade" + "(AU, BR, CA, CH ,DE, DK, ES, FI, " +
                "FR," + "GB, IE, IR, NO, NL, NZ, TR, US):");

            nat = Console.ReadLine();

            ArrayList nat_list = new ArrayList("AU", "BR", "CA", "CH", "DE", "DK",
                "ES", "FI", "FR," + "GB", "IE", "IR", "NO", "NL", "NZ", "TR", "US");

            if (!nat_list.Contains(nat.ToUpper())) nat = "BR";


            Console.WriteLine("Users!");
            string url = "https://randomuser.me/api/" + numero + "&nat=" + nat;
            ConsumindoAPiUsers ConsumindoAPiUsers = BuscarConsumindo(url);

           
            
            Console.ReadLine();

            foreach (Result result in ConsumindoAPiUsers.Results)
            {
                Console.Write("\n RESULTS:\n");
                Console.WriteLine(string.Format("Gender: {0}  \nTitle: {1} \nFirst: {2} \nLast: {3}",
                    result.Gender, result.Name.Title, result.Name.First, result.Name.Last));

                Console.Write("\n LOCATION:\n");
                Console.WriteLine(string.Format("Number: {0}  \nName: {1} \n\nCity: {2} \nState: {3} \nCountry: {4} \nPostcode: {5}   ",
                    result.Location.Street.Number, result.Location.Street.Name, result.Location.City, result.Location.State, result.Location.Country, result.Location.Postcode));

                Console.Write("\n COORDINATES:\n");
                Console.WriteLine(string.Format("Latitude: {0}  \nLongitude: {1} ",
                    result.Location.Coordinates.Latitude, result.Location.Coordinates.Longitude));

                Console.Write("\n TIMEZONE:\n");
                Console.WriteLine(string.Format("Offset: {0}  \nDescription: {1}  ",
                    result.Location.Timezone.Offset, result.Location.Timezone.Description));

                Console.Write("\n EMAIL:\n");
                Console.WriteLine(result.Email);

            }





            Console.ReadLine();



        }


        public static ConsumindoAPiUsers BuscarConsumindo(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Console.WriteLine(content);
            Console.WriteLine("\n\n");
            ConsumindoAPiUsers ConsumindoAPiUsers = JsonConvert.DeserializeObject<ConsumindoAPiUsers>(content);
            return ConsumindoAPiUsers;


        }


    }
    
}
